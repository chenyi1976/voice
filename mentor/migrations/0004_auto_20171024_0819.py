# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-23 21:19
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mentor', '0003_activity_song'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='mentor',
            options={'permissions': (('voice_admin', 'Administrator for Voice'), ('voice_mentor', 'Mentor for Voice'))},
        ),
    ]
