# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from mentor.models import *

admin.site.register(Team)
admin.site.register(Mentor)
admin.site.register(Candidate)
admin.site.register(Activity)
admin.site.register(ActivityDetail)