# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models


class Mentor(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    # name could be duplicated, but mobile should be unique
    mobile = models.CharField(max_length=20, null=True, unique=True)
    detail = models.TextField(null=True)

    def __unicode__(self):
        return 'Mentor ' + self.user.username

    class Meta:
        permissions = (
            ("voice_admin", "Administrator for Voice"),
            ("voice_mentor", "Mentor for Voice"),
        )


class Team(models.Model):
    name = models.CharField(max_length=20)
    mentor = models.ForeignKey(Mentor, null=True, blank=True)
    average_score = models.FloatField(default=0)

    def __unicode__(self):
        return 'Team ' + self.name


class Candidate(models.Model):
    name = models.CharField(max_length=20)
    # name could be duplicated, but mobile should be unique
    mobile = models.CharField(max_length=20, null=True, unique=True)
    team = models.ForeignKey(Team)
    average_score = models.FloatField(default=0)

    def __unicode__(self):
        return 'Candidate ' + self.name


class Activity(models.Model):
    candidate = models.ForeignKey(Candidate)
    song = models.CharField(max_length=30, null=True)
    perform_date = models.DateField(auto_now_add=True)
    average_score = models.FloatField(default=0)

    def __unicode__(self):
        return 'Activity of ' + self.candidate.name + " on " + str(self.perform_date) + ", score " + str(self.average_score)


class ActivityDetail(models.Model):
    activity = models.ForeignKey(Activity)
    mentor = models.ForeignKey(Mentor)
    score = models.PositiveIntegerField(default=0)

    def __unicode__(self):
        return 'Activity of ' + self.activity.candidate.name + " on " + str(self.activity.perform_date) + ", score " + str(self.score)
