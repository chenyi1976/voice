function load_customers() {
    $.ajax({
        url: '/api/customer_search?keyword=' + $('#search_customer_keyword').val(),
        type: "GET",
        success: function (data, textStatus, jqXHR) {
            var customers = jQuery.parseJSON(data.customers_json);
            var customer_div_html = "";
            for (var i = 0; i < customers.length; i++) {
                var customer_name = customers[i].fields.name;
                var customer_id = customers[i].pk;
                customer_div_html += "<div class='fc-event' id='" + customer_id + "'>" + customer_name + "</div>";
            }
            $('#customer_div').html(customer_div_html);
            $('#customer_div .fc-event').each(function () {

                // store data so the calendar knows to render an event upon drop
                $(this).data('event', {
                    title: $.trim($(this).text()), // use the element's text as the event title
                    stick: true // maintain when user navigates (see docs on the renderEvent method)
                });

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#customer_div').html('Fail to load<br>Please retry');
        }
    })
}

function load_events() {
    $.ajax({
        //todo: how to get current date
        url: '/api/job_search?search_date=' + $('#search_customer_keyword').val(),
        type: "GET",
        success: function (data, textStatus, jqXHR) {
            var jobs = jQuery.parseJSON(data.jobs_json);
            var events = [];
            for (var i = 0; i < jobs.length; i++) {
                var started = jobs[i].fields.started;
                //todo: how to get customer name here.
                var customer = jobs[i].fields.customer;
                var job_id = jobs[i].pk;
                if (started)
                    events.push({id: job_id, title: customer, start: started});
            }
            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                events: events,
                editable: true,
                droppable: true,
                drop: function (date) {
                    // alert($(this).text() + date);
                    task = {customer: $(this).get(0).id, date: date};
                    save_new_task(task);
                }
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("load jobs failed")
            // $('#customer_div').html('Fail to load<br>Please retry');
        }
    })

}

function save_new_task(task){
    $.ajax({
        url: '/api/save_new_job?customer=' + task.customer + '&date=' + task.date,
        type: "GET",
        success: function (data, textStatus, jqXHR) {
            alert('saved');
            // var customers = jQuery.parseJSON(data.customers_json);
            // var customer_div_html = "";
            // for (var i = 0; i < customers.length; i++) {
            //     customer_name = customers[i].fields.name;
            //     customer_div_html += "<div class='fc-event'>" + customer_name + "</div>";
            // }
            // $('#customer_div').html(customer_div_html);
            // init_customer_div();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // $('#customer_div').html('Fail to load<br>Please retry');
            alert('failed');
        }
    });

}



function init_calendar() {
    $(function () {
        load_customers();

        load_events();

        $('#search_customer_btn').click(function () {
            load_customers();
        });
    });
}
