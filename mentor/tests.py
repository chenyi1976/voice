# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase, Client

from mentor.helper import *


class HelperScoreTestCase(TestCase):
    def setUp(self):
        pass

    def test_update_activity_score(self):
        pass
        # update_activity_score(act)


class UrlTestCase(TestCase):
    client = Client()

    def setUp(self):
        pass

    def test_teams_url_no_login(self):
        self.client.logout()
        response = self.client.get('/teams')
        self.assertEqual(response.status_code, 302)

    def test_teams_url_with_login(self):
        mentor = fake_mentor_with_team(1)
        self.client.login(username=mentor.user.username, password=mentor.user.username)
        response = self.client.get('/teams')
        self.assertEqual(response.status_code, 200)

    def test_candiate_url_no_login(self):
        self.client.logout()
        mentor = fake_mentor_with_team(1)
        team = mentor.team_set.all()[0]
        fake_candidate_with_activity(team, 1, 1)
        response = self.client.get('/candidate/' + str(team.candidate_set.all()[0].id))
        self.assertEqual(response.status_code, 302)

    def test_candiate_url_with_login(self):
        self.client.logout()
        mentor = fake_mentor_with_team(1)
        team = mentor.team_set.all()[0]
        fake_candidate_with_activity(team, 1, 1)
        self.client.login(username=mentor.user.username, password=mentor.user.username)
        response = self.client.get('/candidate/' + str(team.candidate_set.all()[0].id))
        self.assertEqual(response.status_code, 200)
