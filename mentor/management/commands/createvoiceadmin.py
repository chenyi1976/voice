from __future__ import print_function

from django.contrib.auth.models import Permission
from django.core.management import BaseCommand

from mentor.helper import *


class Command(BaseCommand):
    help = 'Create Voice Admin'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        print('You are now creating administrator for the voice.')
        username = raw_input('username:')
        password = raw_input('password:')
        user = User.objects.create_user(username=username, password=password)
        permission_voice_admin = Permission.objects.get(codename='voice_admin')
        user.user_permissions.add(permission_voice_admin)
        user.save()
