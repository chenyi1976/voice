from __future__ import print_function

from django.core.management import BaseCommand

from mentor.helper import *


class Command(BaseCommand):
    help = 'Create Some Dummy Voice Data'

    def add_arguments(self, parser):
        # parser.add_argument('--size', type=int, default=5)
        pass

    def handle(self, *args, **options):
        # fake_size = options['size']
        # print('fake_size:' + str(fake_size))
        # if fake_size is None:
        #     fake_size = 5

        team_count = None
        mentor_count = None
        candidate_count = None
        activity_count = None
        while mentor_count <= 0:
            try:
                mentor_count = int(raw_input('How many mentors you want to create:'))
            except ValueError:
                print('wrong format, please try again')
        while team_count <= 0:
            try:
                team_count = int(raw_input('How many teams you want to create for each mentor:'))
            except ValueError:
                print('wrong format, please try again')
        while candidate_count <= 0:
            try:
                candidate_count = int(raw_input('How many Candidates you want to create for each team:'))
            except ValueError:
                print('wrong format, please try again')
        while activity_count <= 0:
            try:
                activity_count = int(raw_input('How many Activities you want to create for each team:'))
            except ValueError:
                print('wrong format, please try again')

        # generate data
        fake = Faker()
        teams = []
        for i in range(0, mentor_count):
            mentor = fake_mentor_with_team(team_count, fake)
            teams.extend(mentor.team_set.all())
            print('created mentor ' + mentor.user.username)

        for team in teams:
            for i in range(0, candidate_count):
                fake_candidate_with_activity(team=team, candidate_count=candidate_count, activity_count=activity_count,
                                             fake=fake)
                print('created candidates for team: ' + team.name)
