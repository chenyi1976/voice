from django.db.models.signals import post_save
from django.dispatch import receiver

from mentor.helper import update_activity_score
from mentor.models import *


@receiver(post_save, sender=ActivityDetail)
def activity_save_signal(sender, instance, **kwargs):
    activity = Activity.objects.get(pk=instance.activity.id)
    update_activity_score(activity)


