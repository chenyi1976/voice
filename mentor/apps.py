# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class MentorConfig(AppConfig):
    name = 'mentor'

    def ready(self):
        print 'ready'
        import mentor.signals
