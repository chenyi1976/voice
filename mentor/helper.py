from random import randint

from faker import Faker

from mentor.models import *


def update_team_score(team):
    candidates = team.candidate_set.all()
    total_score = 0.0
    for candidate in candidates:
        total_score += candidate.average_score
    team.average_score = total_score / len(candidates)
    team.save()


def update_candidate_score(candidate):
    activities = candidate.activity_set.all()
    total_score = 0.0
    for activity in activities:
        total_score += activity.average_score
    candidate.average_score = total_score / len(activities)
    candidate.save()

    team = Team.objects.get(pk=candidate.team.id)
    update_team_score(team)


def update_activity_score(activity):
    activity_details = activity.activitydetail_set.all()
    total_score = 0.0
    for activity_detail in activity_details:
        total_score += activity_detail.score
    activity.average_score = total_score / len(activity_details)
    activity.save()

    candidate = Candidate.objects.get(pk=activity.candidate.id)
    update_candidate_score(candidate)


def fake_user(fake=None):
    if fake is None:
        fake = Faker()

    user = None
    while user is None:
        # try until found a name not used.
        fake_user_name = fake.first_name()
        try:
            user = User.objects.create_user(username=fake_user_name,
                                            password=fake_user_name,
                                            first_name=fake_user_name,
                                            last_name=fake.last_name(),
                                            email=fake.email(),
                                            is_staff=True)
        except:
            pass
    return user


def fake_mentor_with_team(team_count, fake=None):
    if fake is None:
        fake = Faker()

    user = fake_user(fake)

    mentor = Mentor(user=user, mobile=fake.phone_number())
    mentor.save()

    for i in range(0, team_count):
        team = Team(name=fake.name(), mentor=mentor)
        team.save()

    return mentor


def fake_candidate_with_activity(team, candidate_count, activity_count, fake=None):
    if fake is None:
        fake = Faker()

    mentors = Mentor.objects.all()

    for i in range(0, candidate_count):
        candidate = Candidate(name=fake.name(), team=team, mobile=fake.phone_number())
        candidate.save()

        # print('created candidate: ' + candidate.name)
        for j in range(0, activity_count):
            activity = Activity(candidate=candidate, perform_date=fake.date_this_decade(),
                                song=fake.name())
            activity.save()

            # print('created activity: ' + candidate.name + ' ' + activity.song)

            for mentor in mentors:
                ad = ActivityDetail(activity=activity, mentor=mentor, score=randint(0, 100))
                ad.save()

