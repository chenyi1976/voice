# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

from mentor.models import *


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect("/")
    else:
        form = UserCreationForm()

    context = {'form': form}
    return render(request, 'registration/register.html', context)


def teams_list(request):
    user = request.user
    if user.has_perm('mentor.voice_admin'):
        teams = Team.objects.all()
    else:
        mentor = Mentor.objects.get(user=user)
        teams = Team.objects.filter(mentor=mentor)

    context = {'user': user,
               'team_list': teams}
    return render(request, 'mentor/team_list.html', context)


def candidate_detail(request, candidate_id):
    candidate = get_object_or_404(Candidate, pk=candidate_id)

    user = request.user
    if not user.is_superuser:
        mentor = Mentor.objects.get(user=user)
        if candidate.team.mentor != mentor:
            raise PermissionDenied

    mentors = Mentor.objects.all()
    context = {'user': user,
               'candidate': candidate,
               'mentors': mentors}
    return render(request, 'mentor/candidate.html', context)
