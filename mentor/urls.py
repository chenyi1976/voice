from django.conf.urls import url
from django.views.generic import RedirectView, DeleteView, DetailView

from mentor.views import *

urlpatterns = [
    url(r'^register/$', register),
    url(r'^$', RedirectView.as_view(url='teams/', permanent=True), name='index'),
    url(r'^teams', teams_list),
    # candidate/id is friendly to server cache
    url(r'^candidate/(?P<candidate_id>[0-9]+)$', candidate_detail),
]
