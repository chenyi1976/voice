### What is this repository for? ###

This is a demo of "The Voice Management System" based on django framework.

#### Some Interesting Stuff ####

* Signal: 'django.db.models.signals' are used for automatically calculation
* Management Commands to generate dummy data: createdummpyvoice
* Management Commands to create administrator user: createvoiceadmin 
* User is controlled via custom permission (administrator user of 'The Voice' should not be system superuser)
* Test Framework to verify url response, good for CI(Continuous Integration)
* Multi settings for both development and production environment, ready for deploy.
* Middleware to control user access

#### Unfinished Stuff ####

* Search Team

#### Things waiting to improve ####

* Middleware to collect exception
* Candidate access could moved to middleware from view itself
* SPA (Single Page Application)
* Restful API with client javascript loading JSON data to populate UI
* Responsive Design
* Data Pagination
* Searching could be server or client side.


#### Used Third Party Library ####

1. Faker (https://github.com/joke2k/faker) is used to generate test data
2. Bootstrap
3. JQuery
4. JQuery UI, in progress
5. Datatables (https://datatables.net/), in progress
6. VueJS (https://vuejs.org/) , in progress
