from defaults import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
THUMBNAIL_DEBUG = True


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'mentor.sqlite3'),
        'TEST': {
            'NAME': os.path.join(BASE_DIR, 'test_mentor.sqlite3'),
        }
    }
}